Microservices app
=========

A simple distributed application that is running across multiple Docker containers.

Getting started
---------------

It uses Docker Compose. So you must have Docker Copose on your sistem

## Linux Containers

The containers use Python, Node.js, Java, Redis for messaging and Postgres for storage.


## Run the app:

```
docker-compose up
```
The app will run at [http://localhost:5000](http://localhost:5000), and the result will be at [http://localhost:5001](http://localhost:5001).



Architecture
-----


* A front-end web app in Python] which lets you vote your favourite programming language
* A Redis that has a queue that colects votes
* A Java processing worker that store the data to the database
* A Postgres database, that has data in volumes
* A Node.js that will show the results in real time


Note
----

This app accepts one vote per client.
